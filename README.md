# Drupal / Angular headless app

Headless App for Drupal - written in Angular.

## Demo:

Front end:
http://mattburton.me.uk/ashfields/app/

Drupal:
http://mattburton.me.uk/ashfields/drupal/

U: **admin**

P: **ashf13ld!**

## Design
The app has three views a 

Bootstrap only list - http://mattburton.me.uk/ashfields/app/#!/events

A grid based list - http://mattburton.me.uk/ashfields/app/#!/grid

An example of how it may integrate with a page at - http://mattburton.me.uk/ashfields/app/#!/inpage


## Installation Instructions:
### Install App 
To install the angular app - clone this repository and run
$bower install - from your terminal 

You will need to be able to alter the app to access the drupal api by changing the path in the factory at app/events/modules/events.services.js 

var allEventsdata = `'/drupal/all_events';` 

### Install Drupal

This repository contains a drupal database marked as Drupal_new.sql

The Drupal installation must have RRESTful Web Services Installed and activated and you can install the Module Rest UI to allow for easy authoring of the correct format JSON array.

**Custom Taxonomy**
The Drupal installation uses a custom Taxonomy - **Event Types** - Which contains a list of Event Types - *Conference, Symposium, etc.* 

**Custom Content Type**
There is also a Custom content type - **Events** Which allows entry of each event into Drupal. This has the custom fields as below.

**Body** - machine name * body *- Text area for description*

**Event Date** - machine name *field_event_date* - Date field

**Event Types** - machine name *field_event_types* - Entry Reference that targets the custom Taxonomy


**Custom View**
I set up a custom view **http://mattburton.me.uk/ashfields/drupal/all-events** Which displays a list of all events. This allows an easy way to manage the API feed to list all views.

This contains a view for the Restful API that exposts the data as:

**Content: Title** - Title

**Content: Body** - Description 

**Content: Event Types** - Comma seperated list of Event Type custom taxonomy

**Content: Event date (Date label)** - Date with custom display set to just show the date

**Content: Event date (Time label)** - Date with custom display set to just show the time

**Content: Event date** - Machine readable Date

The example JSON output from Drupals REST API should look like the following:
http://mattburton.me.uk/ashfields/drupal/all_events 
