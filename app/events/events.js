'use strict';

var eventModule = angular
.module('myAppevents', ['ngRoute','ui.bootstrap'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/events', {
    templateUrl: 'events/events.html',
    controller: 'EventCtrl'
  });
}])
.filter("dateFilterEvents", dateFilterEvents)//Custom date filter for dates between two dates

.controller('EventCtrl', ['$scope', 'eventsData','$timeout','filterFilter', '$filter',   function ($scope, eventsData, $timeout, filterFilter, $filter) {
		getAllevents();
	getInUseTaxonomies();
	$scope.popup1 = { opened: false}; $scope.popup2 = { opened: false};
	$scope.startDate = ""; $scope.endDate = ""; //Inital states for date comparisson 
	$scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
	$scope.altInputFormats = ['M!/d!/yyyy'];
	$scope.orderByField = 'field_event_date';
	$scope.viewby = 5;
	$scope.currentPage = 1;
	$scope.itemsPerPage = $scope.viewby;  
	$scope.selectedIndex = 0; // Whatever the default selected index is, use -1 for no selection
	$scope.letterLimit =70;

		//Get all event data from Drupal feed - save to $scope.eventDatas
		function getAllevents() {
	        eventsData.getAllevents()
	            .then(function (response) {
	                $scope.eventDatas = response.data;
	                if(response){ $scope.totalItems = response.data.length; }  
	            }, function (error) {
	                $scope.warning = 'Unable to load event data: ' + error.message;
	            });
	    }
	    //end get Event Data
    
   

    
	    //Get unique Taxonomies for Categories from Drupal field - save to $scope.allTaxonomies
	   	function getInUseTaxonomies() {
		   	var allTaxonomies = [];
	        eventsData.getAllevents()
	            .then(function (response) {
	                for(var i=0; i < response.data.length; i++){
		               var eachTaxonomy = response.data[i].field_event_types.split(', '); //Include space in split - from Drupal API
		               for (var x = 0; x < eachTaxonomy.length; x++) { 
						   var found = (allTaxonomies.indexOf(eachTaxonomy[x]) > -1); //Check for category in array
						   if(!found){
							   	allTaxonomies.push(eachTaxonomy[x]); //Push to array if not in the array already
						   }	
						}
		               $scope.allTaxonomies = allTaxonomies;
	                } 
	            }, function (error) {
	                $scope.warning = 'Unable to load category data: ' + error.message;
	            });
		}
		// end get Unique taxonomies


	//Angular ui date popups
	$scope.open1 = function() {
    	$scope.popup1.opened = true;
  	};
	$scope.open2 = function() {
    	$scope.popup2.opened = true;
  	};


	//Reset function
	$scope.reset = function() {
    	$scope.startDate = ""; $scope.endDate = ""; $scope.currentPage = 1;
    	delete $scope.queryCategories;
	}

	//Pagination 
	 $scope.setPage = function (pageNo) {
	 	$scope.currentPage = pageNo;
  	};

  	//Change clicked class on grid
  	$scope.itemClicked = function ($index) {
    	$scope.selectedIndex = $index;
  	};
  	//Watch for changes - move to function on app
  	$scope.$watch('queryCategories', function(val) {
	  	if($scope.eventDatas){
        	$scope.filtered = filterFilter($scope.eventDatas, {field_event_types: $scope.queryCategories});
			$scope.totalItems = $scope.filtered.length;
        }
    });


}]);


//Custom filter to show dates between two $scope variables 
function dateFilterEvents() {
	    return function(input, start, end, scope) {
		    if(input){
			        var inputDate = new Date(input),
			            result = [];
			            if(end == ''){ var endDate = new Date('3000/01/01') } else { var endDate = new Date(end) }
						if(start == ''){ var startDate = new Date('1000/01/01') } else { var startDate = new Date(start) }
			        for (var i=0, len = input.length; i < len; i++) {
			            inputDate = new Date(input[i].field_event_date);            
			            if (startDate < inputDate && inputDate < endDate) {
			               result.push(input[i]);
			            }  
			        }       
			     
				return result;
	        }         
	  	};
};
