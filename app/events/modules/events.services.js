eventModule.factory('eventsData', ['$http', function($http) {
	
	var allEventsdata = '/drupal/all_events';
    var eventsData = {};

    eventsData.getAllevents = function () {
        return $http.get(allEventsdata);
    };
    
    eventsData.getTermById = function (val) {
	    var taxonomyTerm = '/drupal/taxonomy/term/'+val+'?_format=json';
        return $http.get(taxonomyTerm);
    };

	return eventsData;

}]);